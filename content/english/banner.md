---
title : "Fiska von Still BLOG"
# full screen navigation
first_name : "Fiska"
last_name : "vonSTILL"
bg_image : "images/backgrounds/full-nav-bg.jpg"
# animated text loop
occupations:
- "Music"
- "Arts"
- "Web Development"
- "Kefir, Combucha & Co"
- "Handicraft"
- "I love Eating - so I love COOKING!"
- "Berlin"

# slider background image loop
slider_images:
- "images/slider/slider-1.jpg"
- "images/slider/slider-2.jpg"
- "images/slider/slider-3.jpg"

# button
button:
enable : true
label : "CONTACT ME"
link : "#contact"


# custom style
custom_class: ""
custom_attributes: ""
custom_css: ""

---
